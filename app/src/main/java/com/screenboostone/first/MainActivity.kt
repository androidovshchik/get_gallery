package com.screenboostone.first

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.text.Editable
import android.text.TextWatcher
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.screenboostone.first.extensions.*
import com.screenboostone.first.utils.getRealPath
import com.snatik.storage.Storage
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.toast
import timber.log.Timber
import java.util.*

class MainActivity : AppCompatActivity(), CoroutineScope {

    private val job = SupervisorJob()

    lateinit var storage: Storage

    @Suppress("DEPRECATION")
    private val dialog by lazy {
        indeterminateProgressDialog(title = "Пожалуйста, подождите...").apply {
            setCancelable(false)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        setContentView(R.layout.activity_main)
        storage = Storage(applicationContext)
        picker.setOnClickListener {
            if (!isGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                requestPermissions(5100, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            } else {
                selectImage()
            }
        }
        input.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable) {
                overlap_text.text = p0.toString()
            }
        })
        save.setOnClickListener {
            if (!isGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                requestPermissions(5200, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            } else {
                saveImage()
            }
        }
    }

    private fun selectImage() {
        startActivityForResult(Intent(Intent.ACTION_GET_CONTENT).apply {
            type = "image/*"
        }.withChooser(applicationContext), 5000)
    }

    @Suppress("DEPRECATION")
    private fun saveImage() {
        val dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).path
        storage.createDirectory(dir)
        val random = Random()
        val path = "$dir${sep}image${random.nextInt(10000..99999)}.png"
        Timber.d("path: $path")
        dialog.show()
        canvas_container.isDrawingCacheEnabled = true
        canvas_container.buildDrawingCache()
        launch {
            val saved = withContext(Dispatchers.IO) {
                canvas_container.drawingCache?.use {
                    Timber.d("bitmap width: ${it.width}")
                    Timber.d("bitmap height: ${it.height}")
                    return@withContext storage.createFile(path, it)
                }
                false
            }
            canvas_container.destroyDrawingCache()
            dialog.hide()
            if (saved) {
                scanFile(path)
                toast("Сохранено как $path")
            } else {
                toast("Не удалось сохранить")
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.getOrNull(0) == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == 5100) {
                selectImage()
            } else if (requestCode == 5200) {
                saveImage()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 5000) {
            if (resultCode != RESULT_OK) {
                return
            }
            val uri = data?.data ?: return
            launch {
                val path = withContext(Dispatchers.IO) {
                    getRealPath(uri)
                } ?: uri.path
                Timber.d("local file path: $path")
                GlideApp.with(applicationContext)
                    .load(path?.toFileUri())
                    .into(background)
            }
        }
    }

    override fun onDestroy() {
        dialog.dismiss()
        coroutineContext.cancelChildren()
        super.onDestroy()
    }

    @Suppress("DEPRECATION")
    override val coroutineContext = Dispatchers.Main + job + CoroutineExceptionHandler { _, e ->
        Timber.e(e)
        dialog.hide()
        toast("$e")
    }
}
