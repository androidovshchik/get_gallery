package com.screenboostone.first

import android.os.AsyncTask
import timber.log.Timber
import java.net.HttpURLConnection
import java.net.URL

open class UrlTask : AsyncTask<Void, Void, String?>() {

    override fun doInBackground(vararg voids: Void): String? {
        return try {
            val url = URL("http://aradgi.ru/fertsone")
            val urlConnection = url.openConnection() as HttpURLConnection
            return urlConnection.inputStream.use {
                it.bufferedReader().readText()
            }
        } catch (e: Exception) {
            Timber.e(e)
            null
        }
    }
}