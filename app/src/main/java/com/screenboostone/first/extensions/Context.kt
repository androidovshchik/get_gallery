/*
 * Copyright (c) 2019. Vlad Kalyuzhnyu <vladkalyuzhnyu@gmail.com>
 */

@file:Suppress("unused")

package com.screenboostone.first.extensions

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaScannerConnection
import androidx.core.content.PermissionChecker.PermissionResult

val Context.allAppPermissions: Array<String>
    get() = packageManager.getPackageInfo(packageName, PackageManager.GET_PERMISSIONS).requestedPermissions
        ?: arrayOf()

@PermissionResult
fun Context.areGranted(vararg permissions: String): Boolean {
    for (permission in permissions) {
        if (!isGranted(permission)) {
            return false
        }
    }
    return true
}

@PermissionResult
fun Context.isGranted(permission: String): Boolean {
    return checkCallingOrSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
}

@Suppress("DEPRECATION")
fun Context.scanFile(path: String) {
    sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).apply {
        data = path.toFileUri()
    })
    MediaScannerConnection.scanFile(applicationContext, arrayOf(path), null, null)
}