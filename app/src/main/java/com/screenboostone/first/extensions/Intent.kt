/*
 * Copyright (c) 2019. Vlad Kalyuzhnyu <vladkalyuzhnyu@gmail.com>
 */

@file:Suppress("unused")

package com.screenboostone.first.extensions

import android.content.Context
import android.content.Intent

fun Intent.withChooser(context: Context): Intent = Intent.createChooser(this, "Выберите приложение")