/*
 * Copyright (c) 2019. Vlad Kalyuzhnyu <vladkalyuzhnyu@gmail.com>
 */

@file:Suppress("unused", "NOTHING_TO_INLINE")

package com.screenboostone.first.extensions

import android.net.Uri

fun String.toUri(): Uri = Uri.parse(this)

fun String.toFileUri(): Uri = Uri.parse("file://$this")